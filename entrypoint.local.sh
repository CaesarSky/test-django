#!/bin/bash
DJANGO_SUPER_USERNAME=${DJANGO_SUPER_USERNAME:-"admin"}
DJANGO_SUPER_USER_EMAIL=${DJANGO_SUPER_USER_EMAIL:-"admin@gmail.com"}
if [ "$( psql -tAc "SELECT 1 FROM pg_database WHERE datname='blog'" )" = "0" ]
then
    echo "Waiting for postgres..."

    while ! nc -z "$SQL_HOST" "$SQL_PORT"; do
      sleep 1
    done

    echo "PostgreSQL started"
fi
echo "Running migrations"

python manage.py flush --no-input
python manage.py migrate
python manage.py admincreate
python manage.py runserver 0.0.0.0:8000

exec "$@"