FROM python:3.8.10-slim-buster

COPY . /app
WORKDIR /app

COPY ./entrypoint.sh .

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUBUFFERED 1

RUN mkdir /app/staticfiles
RUN mkdir /app/mediafiles

RUN pip install pip --upgrade && \
    pip install -r requirements.txt && \
    chmod +x entrypoint.sh

CMD ["/app/entrypoint.sh"]
