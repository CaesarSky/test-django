from django.test import TestCase
from django.contrib.auth.models import User

from .models import Post


class PostTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        testuser1 = User.objects.create_user(
            username='testuser1',password='abc123'
        )
        testuser1.save()

        test_post = Post.objects.create(
            author=testuser1, title='Blog title',
            body='Blog body'
        )
        test_post.save()
    
    def test_blog_content(self):
        post = Post.objects.first()
        self.assertEqual(f'{post.author}', 'testuser1')
        self.assertEqual(f'{post.title}', 'Blog title')
        self.assertEqual(f'{post.body}', 'Blog body')
